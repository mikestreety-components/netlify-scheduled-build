/**
 * Fire the check on both the schedule and when accessed via the URL
 */
 addEventListener('scheduled', event => {
	event.waitUntil(checkBuildRequirement())
});
addEventListener('fetch', async event => {
	event.respondWith(checkBuildRequirement())
});

/**
* Shall we build it?
*/
async function checkBuildRequirement() {

		// Get the pending posts and parse as JSON
	let pending = await fetch(UPCOMING_JSON)
			.then(data => data.json()),

		// Should the build be triggered?
		rebuild = 'No build needed';

	// If we have an item and the date is in the "past"
	if(
		pending &&
		pending.date &&
		(new Date(pending.date) < new Date())
	) {
		rebuild = `Building ${pending.title}`;
		await fetch(BUILD_HOOK, {
			method: 'POST',
		});
	}

	// Return some text
	return new Response(rebuild, {
		headers: {
			'content-type': 'text/plain'
		}
	});
}
