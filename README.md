# Scheduled Netlify builds

- `cp wrangler.toml.example wrangler.toml`
- Fill in wrangler.toml
- `wrangler secret put BUILD_HOOK`
- `wrangler publish`
